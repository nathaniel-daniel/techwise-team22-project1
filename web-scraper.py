import urllib.parse
import time
import asyncio
import aiohttp

from post_database import PostDatabase, PostVerdict
import pushshift_api

USER_AGENT = 'Techwise Team22 Project1 Scraper'
        
class InvalidPostException(Exception):
    def __init__(self, message):
        self.message = message
        super().__init__(self.message)
            
async def get_reddit_post_data(session, url):
    json = None
    async with session.get(url) as response:
        json = await response.json()
        
    comments = json[1]['data']['children']
    
    if len(comments) == 0:
        raise InvalidPostException('Missing Bot Comment')
        
    first_comment = comments[0]
    first_comment_author = first_comment['data']['author']
        
    if first_comment_author != 'Judgement_Bot_AITA':
        raise InvalidPostException(f'Invalid Author `{first_comment_author}`')

    post = json[0]['data']['children'][0]['data']
    comment = first_comment['data']['body']
    
    postbody = post['crosspost_parent_list'][0]["selftext"]
    posturl = post['permalink']
        
    return posturl, postbody, comment
    
# TODO: Should this be a part of get_reddit_post_data?
def parse_post_comment(post_comment):
    VERDICT_TABLE = {
        'Asshole': PostVerdict.A_HOLE,
        'Not the A-hole': PostVerdict.NOT_A_HOLE,
        'Everyone Sucks': PostVerdict.EVERYONE_SUCKS,
        'No A-holes here': PostVerdict.NO_A_HOLES_HERE,
        'Not Enough Info': PostVerdict.NOT_ENOUGH_INFO,
    }
    
    # Parse: The final verdict is: **{verdict}**
    verdict = post_comment.lstrip('The final verdict is: ').split('\n')[0].strip('**')
    
    # TODO: Do we want to parse the cells?
    # for post_comment_line in post_comment.split('\n'):
    #    for cell in post_comment_line.split('|'):
    #        print(cell)
    
    return VERDICT_TABLE[verdict]
    
def parse_aita_submission(submission):
    VERDICT_TABLE = {
        'Asshole': PostVerdict.A_HOLE,
        'Not the A-hole': PostVerdict.NOT_A_HOLE,
        'Everyone Sucks': PostVerdict.EVERYONE_SUCKS,
        'No A-holes here': PostVerdict.NO_A_HOLES_HERE,
        'Not Enough Info': PostVerdict.NOT_ENOUGH_INFO,
        
        'Not enough info': PostVerdict.NOT_ENOUGH_INFO,
        'everyone sucks': PostVerdict.EVERYONE_SUCKS,
        'not the asshole': PostVerdict.NOT_A_HOLE,
        'asshole': PostVerdict.A_HOLE,
        'not the a-hole': PostVerdict.A_HOLE,
        'not enough info': PostVerdict.NOT_ENOUGH_INFO,
        'no a--holes here': PostVerdict.NO_A_HOLES_HERE,
        'no assholes here': PostVerdict.NO_A_HOLES_HERE,
        'obvious asshole': PostVerdict.A_HOLE,
        'asshole for even posting this': PostVerdict.A_HOLE,
        
        '': None,
        'TL;DR': None,
        'UPDATE': None,
        'META': None,
        'Open Forum': None,
        'Talk ENDED': None,
        'Talk LIVE': None,
        'Best of 2021': None,
        'Rule Evading Asshole Supreme': None,
        'Mods Needed!': None,
        'Eat Stew Homie': None,
        'Mod Application!': None,
        'Too confusing': None,
        'too close to call': None,
        'not an AITA post': None,
        'kind of': None,
        'well meaning asshole': None,
        'communicate': None,
        'repost': None,
        'not yet': None,
        'Obvious trolling': None,
        'shitpost': None,
        'Shitpost': None,
        'you right op': None,
        'phrasing!': None,
        'oblivious ': None,
        'wrong sub': None,
        'BEST OF': None,
        'near asshole': None,
        'Kids being kids!': None,
        '(minor) asshole': None,
        'meh': None,
        'teenager': None,
        'More info needed': None,
        'no assholes - yet': None,
        'unreasonable': None,
        'asshole - i guess': None,
        'it\'s complicated': None,
        'dingus': None,
        'not really': None,
        'troll': None,
        'shitposty': None,
        'Let it go': None,
        'shitpost - trolling': None,
        'assTroll': None,
        'too confusing to call': None,
        'nasty message': None,
        'move on': None,
        'cunt': None,
        'asshole, kinda': None,
        'OP trolling': None,
        '90% perfectly ok ': None,
        'too long to read': None,
        'chump, not asshole': None,
        'no results': None,
        'not irrational': None,
        'evasive asshole': None,
        'Troll': None,
        'Mom is right': None,
        'charitable asshole': None,
        'kinda': None,
        'let it go': None,
        'Trolling, don\'t bother': None,
        'interesting': None,
        'asstroll': None,
        'kind of asshole': None,
        'slight asshole': None,
        'immature': None,
        'edgelord': None,
        'clingy': None,
        'tone deaf': None,
        'C\'mon, seriously?': None,
        'kind of harsh': None,
        'RANT': None,
        'What': None,
        'a bit of an asshole': None,
        'not quite': None,
        'jerk': None,
        'of course not': None,
        'just weird': None,
        'COMMUNICATE': None,
        'Lazy': None,
        'Child': None,
        'bit of a twat': None,
        'COMMUNICATE': None,
        'douchenozzle': None,
        'No consensus.': None,
        'OP Trolling': None,
        'affair meddler': None,
        'auto-asshole': None,
        'Shut Up.': None,
        'OP bullshitting': None,
        'teenage drama': None,
        '"overreact-hole"': None,
        'nobody cares': None,
        '1/10th asshole': None,
        'asshole\'s friend': None,
        'sam is an asshole': None,
        'not THE asshole': None,
        'Shit Post': None,
        '#1 is an asshole': None,
        ' META': None,
        '/r/relationships': None,
        'shit post': None,
        'r/relationships': None,
        'LONG Shit Post': None,
        'Announcement': None,
        'aunt with benefits': None,
        'should\'ve towed': None,
        'Announcement': None,
        'dick': None,
        'no responses :(': None,
        'be honest': None,
        'not the a-hole yet': None,
        'negligent': None,
        'dont be a martyr': None,
        'criminal': None,
        'no one cares': None,
        'non issue': None,
        'words not pizza': None,
        'tactless': None,
        'dysfunction': None,
        'not the a-hole but dumbass': None,
        'asshole in theory': None,
        'Na�ve': None,
        'all suck/no info': None,
        'deleter-hole': None,
        'Shit post': None,
        'little dick?': None,
        'not the a-hole?': None,
        'DM;HS asshole': None,
        'potential asshole': None,
        'who cares?': None,
        'assho-ho-ho': None,
        'advice zone': None,
        'bad sitcom family': None,
        'OP is jelly': None,
        'not the a-hole...yet': None,
        'Do an AMA': None,
        'advice seeker': None,
        'half-asshole': None,
        'advice column': None,
        'Advice Seeker': None,
        'terrible situation': None,
        'bit of an asshole': None,
        '[META]': None,
        
        None: None,
    }
    
    url = submission['permalink']
    body = submission['selftext']
    link_flair_text = submission.get('link_flair_text')
    
    verdict = VERDICT_TABLE[link_flair_text]
    
    return url, body, verdict
    
async def get_aita_post(session, url):
    json = None
    async with session.get(url, raise_for_status=True) as response:
        json = await response.json()
        
    return parse_aita_submission(json[0]['data']['children'][0]['data'])
        
async def main():
    SKIP_AITA_FILTERED = True
    SKIP_AITA = False
    
    with PostDatabase() as database:
        if not SKIP_AITA_FILTERED:
            scrape_aita_filtered(database)
        if not SKIP_AITA:
            await scrape_aita(database)
            
        
def scrape_aita_filtered(database):
    LIMIT = 10_000
    INVALID_POST_IS_FATAL = False
    INVALID_STATUS_CODE_IS_FATAL = False
    MAX_POSTS_PER_REQUEST = 500
    
    with requests.Session() as session:
        submission_stream = pushshift_api.stream_all_submissions(session, subreddit='AITAFiltered', stickied=False, size=MAX_POSTS_PER_REQUEST)
        for i, submission in enumerate(submission_stream):
            if i == LIMIT:
                print('Limit reached, exiting...')
                break
                
            url = submission['permalink']
            url = f'https://www.reddit.com{url}'
            api_url = f'{url}.json'
            
            print(f'Scraping `{url}`...')
            if database.has_post(url):
                print('    Post has url, skipping...')
                continue
                
            try:
                post_url, post_body, post_comment = get_reddit_post_data(session, api_url)
                verdict = parse_post_comment(post_comment)
                            
                post_url = f'https://www.reddit.com{post_url}'
                        
                print(f'    Verdict: {verdict}')
                    
                database.insert_post(post_url, post_body, verdict)
            except InvalidPostException as ex:
                if INVALID_POST_IS_FATAL:
                    raise
                else:
                   print(f'    Invalid Post: {ex}')
            except InvalidStatusCodeException as ex:
                if INVALID_STATUS_CODE_IS_FATAL:
                    raise
                else:
                    print(f'    Invalid Status Code: {ex}')
        print('Scraped entire subreddit, exiting...')
    
async def scrape_aita(database):
    NUM_WORKERS = 8
    TASK_QUEUE_SIZE = 1024
    
    # Posts are labelled after 18 hours, add some lee-way
    before = int(time.time()) - (60 * 60 * (18))
    # before = 1598981502 # 1393277671
    # before = None
    
    default_headers = {
        'User-Agent': USER_AGENT
    }

    async with aiohttp.ClientSession(headers=default_headers) as session:
        submission_queue = asyncio.Queue(maxsize=TASK_QUEUE_SIZE)
        submission_fetcher_task = asyncio.create_task(aita_submisson_fetcher_worker(session, submission_queue, before))
        
        workers = []
        for i in range(NUM_WORKERS):
            task = asyncio.create_task(aita_submission_process_worker(database, session, submission_queue))
            workers.append(task)
        
        # Wait for the queue to finish being populated.
        await submission_fetcher_task
        
        # Next, wait for the queue to drain.
        await submission_queue.join()
        
        # Now, we can safely kill workers as they are all done.
        for worker in workers:
            worker.cancel()
            
        # Wait for the workers to exit.
        # NOTE: We expect workers to handle their own exceptions.
        # That means we will NOT attempt to restart dead workers.
        await asyncio.gather(*workers, return_exceptions=True)
        
        # We will either scrape the subreddit, or run into an exception.
        # The exception may be a ctrl+c.
        # In any case, we never make it here unless we scrape the entire subreddit.
        print('Scraped entire subreddit, exiting...')
        
async def aita_submission_process_worker(database, session, queue):
    while True:
        submission = await queue.get()
        try:
            await process_aita_submission(database, session, submission)
        except asyncio.CancelledError:
            raise
        except Exception as ex:
            print(f'worker got exception {ex}, ignoring...')
        queue.task_done()

async def aita_submisson_fetcher_worker(session, queue, before):    
    submission_stream = pushshift_api.stream_all_submissions(
        session, 
        subreddit='AmItheAsshole', 
        stickied=False, 
        size=pushshift_api.SEARCH_SUBMISSIONS_MAX_SIZE, 
        before=before,
    )
    
    async for submission in submission_stream:                
        await queue.put(submission)

async def process_aita_submission(database, session, submission):
    url = submission['permalink']
    url = f'https://www.reddit.com{url}'
    api_url = f'{url}.json'
              
    print(f'Scraping `{url}`...')
    if database.has_aita_post(url):
        print('    DB has url, skipping...')
        return
                
    try:
        post_url, post_body, post_verdict = await get_aita_post(session, api_url)
        post_url = f'https://www.reddit.com{post_url}'
    except aiohttp.ClientResponseError as ex:
        if ex.status == 404:
            print(f'    Got http status 404 for `get_aita_post`, status {ex}, ignoring...')
            return
        else:
            raise
            
    if post_verdict is None:
        print('    Post has not been labelled, skipping...')
        return
                
    print(f'    Verdict: {post_verdict}')
            
    database.insert_aita_post(post_url, post_body, post_verdict)
    
if __name__ == "__main__":
    asyncio.run(main())