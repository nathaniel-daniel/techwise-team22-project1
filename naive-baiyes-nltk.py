from nltk.corpus import twitter_samples
from nltk.corpus import stopwords
from nltk.tag import pos_tag
from nltk.stem.wordnet import WordNetLemmatizer
import re, string
from post_database import PostDatabase, PostVerdict
from nltk.tokenize import word_tokenize
from nltk import FreqDist
import random
from nltk import classify
from nltk import NaiveBayesClassifier
from itertools import chain

def get_posts_for_model(cleaned_tokens_list, all_words):
    for post_tokens in cleaned_tokens_list:
        value = dict([token, True] for token in post_tokens)
        for word in all_words:
            value.setdefault(word, False)
        yield value

def remove_noise(post_tokens, stop_words = ()):

    cleaned_tokens = []

    for token, tag in pos_tag(post_tokens):
        token = re.sub('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+#]|[!*\(\),]|'\
                       '(?:%[0-9a-fA-F][0-9a-fA-F]))+','', token)
        token = re.sub("(@[A-Za-z0-9_]+)","", token)

        if tag.startswith("NN"):
            pos = 'n'
        elif tag.startswith('VB'):
            pos = 'v'
        else:
            pos = 'a'

        lemmatizer = WordNetLemmatizer()
        token = lemmatizer.lemmatize(token, pos)

        if len(token) > 0 and token not in string.punctuation and token.lower() not in stop_words:
            cleaned_tokens.append(token.lower())
    return cleaned_tokens

def lemmatize_sentence(tokens):
    lemmatizer = WordNetLemmatizer()
    lemmatized_sentence = []
    for word, tag in pos_tag(tokens):
        if tag.startswith('NN'):
            pos = 'n'
        elif tag.startswith('VB'):
            pos = 'v'
        else:
            pos = 'a'
        lemmatized_sentence.append(lemmatizer.lemmatize(word, pos))
    return lemmatized_sentence

def get_all_words(cleaned_tokens_list):
    for tokens in cleaned_tokens_list:
        for token in tokens:
            yield token

def iter_processed_posts(post_iter, stop_words=None):
    for (url, body, verdict) in post_iter:
        classification = None
        if verdict == PostVerdict.A_HOLE:
            # classification = 'YTA'
            classification = True
        elif verdict == PostVerdict.NOT_A_HOLE:
            # classification = 'NTA'
            classification = False
        else:
            raise Exception(f'Unknown verdict "f{verdict}"')
            
        # tokens = word_tokenize(body)
        # removed_noise = remove_noise(tokens, stop_words)
        
        # features = dict([token, True] for token in removed_noise)
        # for word in all_words:
        #     features.setdefault(word, False)
        
        #yield (features, classification)
        
        yield (body, int(classification))

def create_post_iter(database):
    ahole_post_iter = database.iter_aita_posts(filter_invalid=True, verdicts=[PostVerdict.A_HOLE])
    not_ahole_post_iter = database.iter_aita_posts(filter_invalid=True, verdicts=[PostVerdict.NOT_A_HOLE])
    post_iter = interleave(ahole_post_iter, not_ahole_post_iter)
    
    processed_posts_iter = iter_processed_posts(post_iter)
    
    return post_iter

def interleave(iter1, iter2):
    while True:
        try:
            value1 = next(iter1)
            value2 = next(iter2)
            
            yield value1 
            yield value2 
        except StopIteration:
            return

def main():
    stop_words = stopwords.words('english')
   
    with PostDatabase() as database:
        yta_posts = []
        nta_posts = []
        processed_posts_iter = create_post_iter(database)
        for (i, (url, body, verdict)) in enumerate(processed_posts_iter):
            if verdict not in [PostVerdict.A_HOLE, PostVerdict.NOT_A_HOLE]:
                continue
                
            if i % 100 == 0:
                print(f'Progress: {i}')
                
            if i == 15_000:
                break
            #if i >= 15_000:
            #    break

            if verdict == PostVerdict.A_HOLE:
                yta_posts.append(word_tokenize(body))
            elif verdict == PostVerdict.NOT_A_HOLE:
                nta_posts.append(word_tokenize(body))
            else:
                raise Exception(f'Unknown verdict "f{verdict}"')
                
        print('Cleaning dataset...')
        yta_cleaned = []
        nta_cleaned = []
        for tokens in yta_posts:
            yta_cleaned.append(remove_noise(tokens, stop_words))

        for tokens in nta_posts:
            nta_cleaned.append(remove_noise(tokens, stop_words))
       
    print('Compiling all words list...')
    all_words = set()
    for word_list in chain(yta_cleaned, nta_cleaned):
        for word in word_list:
            all_words.add(word)
       
    yta_model = get_posts_for_model(yta_cleaned, all_words)
    nta_model = get_posts_for_model(nta_cleaned, all_words)

    print('Transforming dataset...')
    yta_dataset = []
    for feature_dict in yta_model:
        yta_dataset.append((feature_dict, "YTA"))
        
    nta_dataset = []
    for feature_dict in nta_model:
        nta_dataset.append((feature_dict, "NTA"))

    dataset = yta_dataset + nta_dataset

    print('Shuffling dataset...')
    random.shuffle(dataset)

    partition = int(len(dataset) * 0.7)
    
    print(f'Partition: {partition}')
    
    train_data = dataset[:partition]
    test_data = dataset[partition:]

    print('Training...')
    classifier = NaiveBayesClassifier.train(train_data)

    print("Accuracy is:", classify.accuracy(classifier, test_data))

    classifier.show_most_informative_features(20)
             
    
if __name__ == '__main__':
    main()

            
