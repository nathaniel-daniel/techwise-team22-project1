from wordcloud import WordCloud
import json
import itertools

from post_database import PostDatabase, PostVerdict
from tokenizer import LemmaTokenizer
from util import interleave

from sklearn.feature_extraction import text

def iter_processed_posts(post_iter, stop_words=None):
    for (url, body, verdict) in post_iter:
        classification = None
        if verdict == PostVerdict.A_HOLE:
            classification = True
        elif verdict == PostVerdict.NOT_A_HOLE:
            classification = False
        else:
            raise Exception(f'Unknown verdict "f{verdict}"')
        
        yield (body, int(classification))

def create_post_iter(database):
    ahole_post_iter = database.iter_aita_posts(filter_invalid=True, verdicts=[PostVerdict.A_HOLE])
    not_ahole_post_iter = database.iter_aita_posts(filter_invalid=True, verdicts=[PostVerdict.NOT_A_HOLE])
    post_iter = interleave(ahole_post_iter, not_ahole_post_iter)
    
    processed_posts_iter = iter_processed_posts(post_iter)
    
    return processed_posts_iter
        

def update_freq_dict(dict, tokens):
    for token in tokens:
        #if token in text.ENGLISH_STOP_WORDS:
        #    continue
            
        if token in dict:
            dict[token] += 1
        else:
            dict[token] = 1
            
def sort_frequency_list(freq_list):
    return dict(sorted(freq_list.items(), key=lambda item: item[1], reverse=True))
            
def main():
    LIMIT = 1_000_000
    
    tokenizer = LemmaTokenizer()
    
    with PostDatabase(optimize_on_close=False) as database:
        print('Generating frequency lists...')
        freq_list_yta = dict()
        freq_list_nta = dict()
        
        post_iter = create_post_iter(database)
        for (body, classification) in itertools.islice(post_iter, LIMIT):
            tokens = tokenizer(body)
            
            if bool(classification):
                update_freq_dict(freq_list_yta, tokens)
            else:
                update_freq_dict(freq_list_nta, tokens)
                
        print('Saving frequency lists...')
        with open('freq-list-yta.json', 'w') as file:
            json.dump(sort_frequency_list(freq_list_yta), file, indent=4)
        with open('freq-list-nta.json', 'w') as file:
            json.dump(sort_frequency_list(freq_list_nta), file, indent=4)
                    
        
        print("Generating wordcloud...")
        wordcloud_yta = WordCloud(width=1600, height=800, stopwords=set(), relative_scaling=1, max_words=500)
        wordcloud_nta = WordCloud(width=1600, height=800, stopwords=set(), relative_scaling=1, max_words=500)
        wordcloud_yta.generate_from_frequencies(freq_list_yta)
        wordcloud_nta.generate_from_frequencies(freq_list_nta)
        
        print('Saving wordcloud...')
        wordcloud_yta.to_file('wordcloud-yta.png')
        wordcloud_nta.to_file('wordcloud-nta.png')

if __name__ == '__main__':
    main()
