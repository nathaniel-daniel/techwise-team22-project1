from nltk.stem.wordnet import WordNetLemmatizer
from nltk.tokenize import word_tokenize
from nltk.tag import pos_tag
from nltk.corpus import wordnet

import string

import contractions

INVALID_TOKENS = {
    '\u2018',
    '\u2019',
    '\u201d',
    '\u201c',
    '``',
    '\'\'',
}

def get_wordnet_pos(treebank_tag):
    if treebank_tag.startswith('J'):
        return wordnet.ADJ
    elif treebank_tag.startswith('V'):
        return wordnet.VERB
    elif treebank_tag.startswith('N'):
        return wordnet.NOUN
    elif treebank_tag.startswith('R'):
        return wordnet.ADV
    else:
        # print('Unknown Treebank Tag: ' + treebank_tag)
        return None
        
REPLACE_MAP = {
    # Note: May be posessive
    'kid\'s': 'kid is',
    'parent\'s': 'parent is',
    'son\'s': 'son is',
    'potato\'s': 'potato is',
    'cindi\'s': 'cindi is',
    'brother\'s': 'brother is',
    'mother\'s': 'mother is',
    'friend\'s': 'friend is',
    'mom\'s': 'mom is',
    'groom\'s': 'groom is',
    'grandma\'s': 'grandma is',
    'bride\'s': 'bride is',
    'lion\'s': 'lion is',
    'grandfather\'s': 'grandfather is',
    'sister\'s': 'sister is',
    'aunt\'s': 'aunt is',
    'rosie\'s': 'rosie is',
    'cabin\'s': 'cabin is',
    'uncle\'s': 'uncle is',
    'other\'s': 'other is',
    'fiance\'s': 'fiance is',
    'dad\'s': 'dad is',
    'neightbor\'s': 'neightbor is',
    'year\'s': 'year is',
    'wife\'s': 'wife is',
    'daughter\'s': 'daughter is',
    'rosa\'s': 'rosa is',
    'husband\'s': 'husband is',
    'stranger\'s': 'stranger is',
    'bf\'s': 'boyfriend is',
    'story\'s': 'story is',
    'tonight\'s': 'tonight is',
    'neighbor\'s': 'neighbor is',
    'nick\'s': 'nick is',
    'boss\'s': 'boss is',
    'child\'s': 'child is',
    'week\'s': 'week is',
    'doctor\'s': 'doctor is',
    'mommy\'s': 'mommy is',
    'college\'s': 'college is',
    'puppy\'s': 'puppy is',
    'law\'s': 'law is',
    'client\'s': 'client is',
    'man\'s': 'man is',
    'father\'s': 'father is',
    'girl\'s': 'girl is',
    'dog\'s': 'dog is',
    'drake\'s': 'drake is',
    'woman\'s': 'woman is',
    'farmer\'s': 'farmer is',
    'gf\'s': 'girlfriend is',
    'jane\'s': 'jane is',
    'daddy\'s': 'daddy is',
    'kelly\'s': 'kelly is',
    'office\'s': 'office is',
    'niece\'s': 'niece is',
    'baby\'s': 'baby is',
    'member\'s': 'member is',
    'cousin\'s': 'cousin is',
    'guy\'s': 'guy is',
    'lucy\'s': 'lucy is',
    'mum\'s': 'mum is',
    'robin\'s': 'robin is',
    'litter\'s': 'litter is',
    'laura\'s': 'laura is',
    'life\'s': 'life is',
    'ava\'s': 'ava is',
    'dude\'s': 'dude is',
    'clarity\'s': 'clarity is',
    'school\'s': 'school is',
    'partner\'s': 'partner is',
    'else\'s': 'else is',
    'one\'s': 'one is',
    'kim\'s': 'kim is',
    'god\'s': 'god is',
    'rachel\'s': 'rachel is',
    'cat\'s': 'cat is',
    'landlord\'s': 'landlord is',
    'family\'s': 'family is',
    'teen\'s': 'teen is',
    'team\'s': 'team is',
    'kate\'s': 'kate is',
    'thor\'s': 'thor is',
    'isabel\'s': 'isabel is',
    'jack\'s': 'jack is',
    'thorne\'s': 'thorne is',
    'friday\'s': 'friday is',
    'mia\'s': 'mia is',
    'ashlee\'s': 'ashlee is',
    'relationship\'s': 'relationship is',
    'rowan\'s': 'rowan is',
    'manga\'s': 'manga is',
    'elizabeth\'s': 'elizabeth is',
    'justin\'s': 'justin is',
    'teacher\'s': 'teacher is',
    'couple\'s': 'couple is',
    'fiancé\'s': 'fiance is',
    'schooler\'s': 'schooler is',
    'samantha\'s': 'samantha is',
    'juliana\'s': 'juliana is',
    'grandpa\'s': 'grandpa is',
    'ella\'s': 'ella is',
    'joseph\'s': 'joseph is',
    'elise\'s': 'elise is',
    'everybody\'s': 'everybody is',
    'day\'s': 'day is',
    'jack\'s': 'jack is',
    'deondre\'s': 'deondre is',
    'shelf\'s': 'shelf is',
    'field\'s': 'field is',
    'morning\'s': 'morning is',
    'candace\'s': 'candace is',
    'anne\'s': 'anne is',
    'lily\'s': 'lily is',
    'mark\'s': 'mark is',
    'master\'s': 'master is',
    'layla\'s': 'layla is',
    'roommate\'s': 'roomate is',
    'shampoo\'s': 'shampoo is',
    'becky\'s': 'becky is',
    'mr.g\'s': 'mr.g is',
    'vine\'s': 'vine is',
    'sarah\'s': 'sarah is',
    'penny\'s': 'penny is',
    'george\'s': 'george is',
    'millie\'s': 'millie is',
    'party\'s': 'party is',
    'customer\'s': 'customer is',
    'stylist\'s': 'stylist is',
    'jackie\'s': 'jackie is',
    'colleague\'s': 'colleague is',
    'group\'s': 'group is',
    'patty\'s': 'patty is',
    'wendy\'s': 'wendy is',
    'brenda\'s': 'brenda is',
    'sis\'s': 'sis is',
    'fiancée\'s': 'fiancee is',
    'honor\'s': 'honor is',
    'alice\'s': 'alice is',
    'range\'s': 'range is',
    'toby\'s': 'toby is',
    'ashley\'s': 'ashley is',
    'nephew\'s': 'nephew is',
    'ace\'s': 'ace is',
    'olivia\'s': 'olivia is',
    'sue\'s': 'sue is',
    'charlie\'s': 'charlie is',
    'blake\'s': 'blake is',
    'david\'s': 'david is',
    'amy\'s': 'amy is',
    'conscience\'s': 'conscience is',
    'dani\'s': 'dani is',
    'relative\'s': 'relative is',
    'emma\'s': 'emma is',
    'jimmy\'s': 'jimmy is',
    'vip\'s': 'vip is',
    'bachelor\'s': 'bachelor is',
    'corey\'s': 'corey is',
    'student\'s': 'student is',
    'anybody\'s': 'anybody is',
    'leo\'s': 'leo is',
    'mira\'s': 'mira is',
    'smith\'s': 'smith is',
    'sean\'s': 'sean is',
    'city\'s': 'city is',
    'sam\'s': 'sam is',
    'owen\'s': 'owen is',
    'mary\'s': 'mary is',
    'jacob\'s': 'jacob is',
    'jerry\'s': 'jerry is',
    'valerie\'s': 'valerie is',
    'adam\'s': 'adam is',
    'driver\'s': 'driver is',
    'satan\'s': 'satan is',
    'acquaintance\'s': 'acquaintance is',
    'michelle\'s': 'michelle is',
    'reddit\'s': 'reddit is',
    
    # This is always posessive
    'neighbours\'': 'neighbours',
    'girls\'': 'girls',
    'women\'s': 'women',
    'men\'s': 'men',
    'people\'s': 'people',
    'children\'s': 'children',
    'mice\'s': 'mice',
    'grandparents\'': 'grandparents',
    'boys\'': 'boys',
    'classmates\'': 'classmates',
    'buddies\'': 'buddies',
    'parents\'': 'parents',
    'her\'s': 'her',
    'porches\'': 'porches',
    'dentists\'': 'dentists',
    'kids\'': 'kids',
    'sons\'': 'sons',
    'others\'': 'others',
    'friends\'': 'friend',
    'clients\'': 'clients',
    'adults\'': 'adults',
    
    # Misspelling
    'neigbor\'s': 'neighbor is',
    'you\'r': 'you are',
    'arn\'t': 'are not',
    'didin\'t': ' did not',
    
    # HTML Escapes
    '#x200b;': '', # This is a zero width joiner and is "invisible"
    '&amp;': '&',
    
    # Uncategorized
    'everything\'s': 'everything is', # Note: can be "everything has"
    'today\'s': 'today is',
    'mother-in-law\'s': 'mother in law\'s',
    'c\'mon': 'come on',
    'she\'s': 'she is',
    'nothing\'s': 'nothing is',
    'f\'ing': 'fucking',
    'y\'know': 'you know',
    'freezin\'': 'freezing',
    'i\'m': 'i am',
    'comin\'': 'coming',
    'bff\'s': 'bff is',
    '\u00e9': 'e',
}

class LemmaTokenizer:
    def __init__(self, stop_words = []):
        self.wnl = WordNetLemmatizer()
        self.stop_words = set(stop_words)
        
    def __call__(self, doc):
        doc_lower = doc.lower()        
        doc_lower = contractions.fix(doc_lower)
        
        for (k, v) in REPLACE_MAP.items():
            doc_lower = doc_lower.replace(k, v)

        """
        try:
            i = doc_lower.index('\'')
        except ValueError:
            i = -1
            
        if i != -1:
            try:
                l_space = doc_lower[:i].rindex(' ')
            except ValueError:
                l_space = 0
                
            try:
                r_space = doc_lower[i:].index(' ') + i
            except ValueError:
                r_space = len(doc_lower)
                
            print('Line: ', doc_lower[l_space:r_space])
        """
        
        tokens = []
        for token, tag in pos_tag(word_tokenize(doc_lower)):
            if token in string.punctuation or token in INVALID_TOKENS:
                continue
            
            if token not in self.stop_words:
                wordnet_pos = get_wordnet_pos(tag)
                if wordnet_pos:
                    tokens.append(self.wnl.lemmatize(token, wordnet_pos))
                else:
                    tokens.append(token)
                
                """
                if 'wa' in token:
                    print('WA: ', token)
                """
                
        return tokens