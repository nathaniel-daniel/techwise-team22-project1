import asyncio
import aiohttp

# See https://github.com/pushshift/api


SEARCH_SUBMISSIONS_MAX_SIZE = 500
"""
The max size for `search_submissions` according to pushshift's docs, which is 500.

I am fairly certain the docs are lying.
I have never been able to get more than 250 results at once.
"""

async def search_submissions(session, subreddit=None, stickied=None, size=None, before=None):
    """
    Search for reddit submissions.
    The `size` parameter must be <= 500.
    """
    
    URL = 'https://api.pushshift.io/reddit/search/submission/'
    params = {}
    if subreddit is not None:
        params['subreddit'] = str(subreddit)
    if stickied is not None:
        params['stickied'] = "true" if stickied else "false"
    if size is not None:
        params['size'] = int(size)
    if before is not None:
        params['before'] = before
    
    json = None
    retry_counter = 0
    
    while True:
        try:
            async with session.get(URL, params=params, raise_for_status=True) as response:
                json = await response.json()
            break
        except aiohttp.ClientResponseError:
            if retry_counter < 3:
                retry_counter += 1
                await asyncio.sleep(2 ** retry_counter)
            else:
                raise
        
    return json

async def stream_all_submissions(session, subreddit=None, stickied=None, size=None, before=None):
    """
    Return an async generator over reddit submissions, going backwards in time.
    """
    
    should_exit = False
    while not should_exit:
        json = await search_submissions(session, subreddit, stickied, size, before)
        
        for submission in json['data']:
            before = submission['created_utc']
            yield submission
            
        if len(json['data']) == 0:
            break