def interleave(iter1, iter2):
    """
    Return a generator that interleaves the outputs of 2 iterators.
    Stops yielding elements after any iterator is exhausted.
    """
    
    while True:
        try:
            value1 = next(iter1)
            value2 = next(iter2)
            
            yield value1 
            yield value2 
        except StopIteration:
            return
            
def batch(size, iter):
    """
    Returns a generator that yields items in lists of size.
    The last list may have a size smaller than size.
    """
    
    buffer = []
    for item in iter:
        buffer.append(item)
        if len(buffer) >= size:
            yield buffer
            buffer = []            
            
    if len(buffer) > 0:
        yield buffer
        buffer = []