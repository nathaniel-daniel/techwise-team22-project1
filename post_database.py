import sqlite3
import enum

DATABASE_NAME = 'data.db'

INIT_DATABASE_SQL = """
PRAGMA foreign_keys = ON;
PRAGMA journal_mode = WAL;
PRAGMA synchronous = NORMAL;

CREATE TABLE IF NOT EXISTS raw_post_data (
    url TEXT PRIMARY KEY UNIQUE NOT NULL CHECK(typeof(url) = 'text'), 
    body TEXT NOT NULL CHECK(typeof(body) = 'text'),
    
    -- See `PostVerdict` for this field's meaning
    final_verdict INTEGER NOT NULL CHECK(typeof(final_verdict) = 'integer')
);

CREATE TABLE IF NOT EXISTS aita_post_data (
    url TEXT PRIMARY KEY UNIQUE NOT NULL CHECK(typeof(url) = 'text'), 
    body TEXT NOT NULL CHECK(typeof(body) = 'text'),
    
    -- See `PostVerdict` for this field's meaning
    final_verdict INTEGER NOT NULL CHECK(typeof(final_verdict) = 'integer')
);
"""

HAS_POST_SQL = """
SELECT EXISTS (
    SELECT 
        1 
    FROM 
        raw_post_data 
    WHERE 
        url = ?
);
"""

HAS_AITA_POST_SQL = """
SELECT EXISTS (
    SELECT 
        1 
    FROM 
        aita_post_data 
    WHERE 
        url = ?
);
"""

INSERT_POST_SQL = """
INSERT INTO raw_post_data (
    url,
    body,
    final_verdict
) VALUES (
    ?,
    ?,
    ?
);
"""

INSERT_AITA_POST_SQL = """
INSERT INTO aita_post_data (
    url,
    body,
    final_verdict
) VALUES (
    ?,
    ?,
    ?
);
"""

@enum.unique
class PostVerdict(enum.Enum):
    A_HOLE = 1
    NOT_A_HOLE = 2
    EVERYONE_SUCKS = 3
    NO_A_HOLES_HERE = 4
    NOT_ENOUGH_INFO = 5

class PostDatabase:
    def __init__(self, optimize_on_close=True):
        self.connection = sqlite3.connect(DATABASE_NAME)
        self.connection.row_factory = sqlite3.Row
        
        self.connection.executescript(INIT_DATABASE_SQL)
        self.connection.commit()
        
        self.cursors = []
        self.optimize_on_close = optimize_on_close
        
    def __enter__(self):
        return self

    def __exit__(self, ex_type, ex_value, ex_traceback):        
        # Close unfinished transactions
        for cursor in self.cursors:
            cursor.close()
            cursor.id = 0
        self.cursors = []
        
        # Don't auto-commit transactions
        self.connection.rollback()
        
        # Cleaning and optimization
        if self.optimize_on_close:
            self.connection.execute('PRAGMA OPTIMIZE;')
            self.connection.execute('VACUUM;')
            self.connection.commit()
        
        self.connection.close()
        
    def has_post(self, url):
        row = self.connection.execute(HAS_POST_SQL, (url,)).fetchone()
        if row is None:
            raise Exception('failed to check if post exists')
        return bool(row[0])
        
    def has_aita_post(self, url):
        row = self.connection.execute(HAS_AITA_POST_SQL, (url,)).fetchone()
        if row is None:
            raise Exception('failed to check if post exists')
        return bool(row[0])
        
    def insert_post(self, url, body, verdict):
        with self.connection as connection:
            self.connection.execute(INSERT_POST_SQL, (
                url, 
                body, 
                verdict.value
            ))
            
    def insert_aita_post(self, url, body, verdict):
        with self.connection as connection:
            self.connection.execute(INSERT_AITA_POST_SQL, (
                url, 
                body, 
                verdict.value
            ))
            
    def iter_posts(self, filter_invalid=True, verdicts=None):        
        cursor = PostCursor(self.connection.cursor())
        cursor.execute(generate_iter_posts_sql('raw_post_data', filter_invalid=filter_invalid, verdicts=verdicts))
            
        for row in cursor:
            yield (row['url'], row['body'], PostVerdict(row['final_verdict']))
            
    def iter_aita_posts(self, filter_invalid=True, verdicts=None):
        cursor = PostCursor(self)
        cursor.execute(generate_iter_posts_sql('aita_post_data', filter_invalid=filter_invalid, verdicts=verdicts))
            
        for row in cursor:
            yield (row['url'], row['body'], PostVerdict(row['final_verdict']))
            
#    def count_aita_filtered_posts():
    def count_aita_posts(self, filter_invalid=True, verdicts=None):
        row = self.connection.execute(generate_count_posts_sql('aita_post_data', filter_invalid=filter_invalid, verdicts=verdicts)).fetchone()
        if row is None:
            raise Exception('failed to count posts')
        return int(row[0])
    
def generate_iter_posts_sql(table_name, filter_invalid=True, verdicts=None):
    has_verdicts = verdicts is not None
    
    query = ''
    query += 'SELECT\n' 
    query += '  url,\n' 
    query += '  body,\n'
    query += '  final_verdict\n'
    
    query += 'FROM\n'
    query += f'  {table_name}\n'
    
    if filter_invalid or has_verdicts:
        query += 'WHERE\n'
        if filter_invalid:
            query += '  body NOT IN ("[deleted]", "[removed]") '
            if has_verdicts:
                query += 'AND'
            query += '\n'
        if has_verdicts:
            query += '  final_verdict IN (' + ', '.join(str(verdict.value) for verdict in verdicts) + ')\n'
    
    query += 'ORDER BY\n'
    query += '  url DESC;'
    
    return query
  
def generate_count_posts_sql(table_name, filter_invalid=True, verdicts=None):
    has_verdicts = verdicts is not None

    query = ''
    query += 'SELECT\n'
    query += '  COUNT(*)\n'
    query += 'FROM\n'
    query += f'  {table_name}\n'
    if filter_invalid or has_verdicts:
        query += 'WHERE\n'
        if filter_invalid:
            query += '  body NOT IN ("[deleted]", "[removed]") '
            if has_verdicts:
                query += 'AND'
            query += '\n'
        if has_verdicts:
            query += '  final_verdict IN (' + ', '.join(str(verdict.value) for verdict in verdicts) + ')\n'
    query += ';'
    
    return query
    
class PostCursor:
    id_generator = 0
    
    def __init__(self, post_database):
        self.post_database = post_database
        self.cursor = post_database.connection.cursor()
        
        PostCursor.id_generator += 1
        self.id = PostCursor.id_generator
        
        self.post_database.cursors.append(self)
        
    def __del__(self):
       self.cleanup()
        
    def cleanup(self):
       if self.id != 0:
           self.post_database.cursors.remove(self)
           self.id = 0
       
    def execute(self, sql, parameters=()):
        return self.cursor.execute(sql, parameters)
        
    def close(self):
        return self.cursor.close()
        
    def __iter__(self):
        return self.cursor.__iter__()
        
    def __eq__(self, other):
        return self.id == other.id
