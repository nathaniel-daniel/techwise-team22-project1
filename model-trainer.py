from post_database import PostDatabase, PostVerdict
from tokenizer import LemmaTokenizer
from util import interleave, batch

import json
import random
import itertools
import re
import string
import time
import pickle
import numpy as np

from sklearn import preprocessing
from sklearn.feature_extraction import DictVectorizer
from sklearn.feature_extraction.text import HashingVectorizer, CountVectorizer, TfidfTransformer
from sklearn.naive_bayes import MultinomialNB, BernoulliNB
from sklearn.linear_model import PassiveAggressiveClassifier, SGDClassifier
from sklearn.pipeline import Pipeline
    
def iter_processed_posts(post_iter, stop_words=None):
    for (url, body, verdict) in post_iter:
        classification = None
        if verdict == PostVerdict.A_HOLE:
            classification = True
        elif verdict == PostVerdict.NOT_A_HOLE:
            classification = False
        else:
            raise Exception(f'Unknown verdict "f{verdict}"')
        
        yield (body, int(classification))

def create_post_iter(database):
    ahole_post_iter = database.iter_aita_posts(filter_invalid=True, verdicts=[PostVerdict.A_HOLE])
    not_ahole_post_iter = database.iter_aita_posts(filter_invalid=True, verdicts=[PostVerdict.NOT_A_HOLE])
    post_iter = interleave(ahole_post_iter, not_ahole_post_iter)
    
    processed_posts_iter = iter_processed_posts(post_iter)
    
    return processed_posts_iter

def create_count_vectorizer(database):
    def make_body_iter(iter):
        for (i, (body, classification)) in enumerate(iter):
            if i % 100 == 0:
                print(f'CountVectorizer Training Iteration: {i}')
            yield body
            
    vectorizer = CountVectorizer(
        strip_accents='unicode', 
        tokenizer=LemmaTokenizer(),
        ngram_range=(1,4), 
        min_df=float(0.0005),
        max_df=float(0.999),
    )
    vectorizer = Pipeline([
        ('count', vectorizer), 
        #('tfid', TfidfTransformer()),
    ])
    post_iter = create_post_iter(database)
    body_iter = make_body_iter(post_iter)
    
    vectorizer.fit(body_iter)
       
    return vectorizer
  
def create_hashing_vectorizer():
    print("Getting Frequency Lists...")
    my_stop_words = []
    with open('freq-list-yta.json', 'rb') as file:
        freq_list_yta=json.load(file)
    with open('freq-list-nta.json', 'rb') as file:
        freq_list_nta=json.load(file)

    print("Generating Stop Words...")
    for word, freq in freq_list_nta.items():
        #if freq > 8000:
        #    my_stop_words.append(str(word))
        if freq < 200:
            my_stop_words.append(str(word))
        
    #for word, freq in freq_list_yta.items():
    #    if freq > 5000:
    #        my_stop_words.append(str(word))
    #   if freq < 300:
    #        my_stop_words.append(str(word))
    
    #my_stop_words = text.ENGLISH_STOP_WORDS.union(["told",'said','say','know','think','now','want','thing','going','make'])
    #my_stop_words = ["told",'said','say','know','think','now','want','thing','going','make']
    #my_stop_words = ["told",'said','say','know','think','now','want','thing','going','make',"wa",'s','.',"'","n't","like", "did", "just",")", "(","ha","'s","","''",",","\u2019"]

    vectorizer = HashingVectorizer(
        n_features=2 ** 18, 
        # strip_accents='ascii', 
        strip_accents='unicode',
        alternate_sign=True, 
        ngram_range=(1,3), 
        # stop_words=my_stop_words,
        tokenizer = LemmaTokenizer(stop_words=my_stop_words),
    )
        
    return vectorizer
    
def calc_max_dataset_size(database):
    num_a_hole = database.count_aita_posts(filter_invalid=True, verdicts=[PostVerdict.A_HOLE])
    num_not_a_hole = database.count_aita_posts(filter_invalid=True, verdicts=[PostVerdict.NOT_A_HOLE])
    return min(num_a_hole, num_not_a_hole) * 2

def main():
    DATASET_SIZE_LIMIT = 100_000
    
    TRAIN_SET_SIZE = int(DATASET_SIZE_LIMIT * 0.7)
    TEST_SET_SIZE = DATASET_SIZE_LIMIT - TRAIN_SET_SIZE
    
    print(f'Train Set Size: {TRAIN_SET_SIZE}')
    print(f'Test Set Size: {TEST_SET_SIZE}')
    print()

    with PostDatabase(optimize_on_close=False) as database:        
        max_dataset_size = calc_max_dataset_size(database)
        print(f'Max Dataset Size: {max_dataset_size}')
        assert(DATASET_SIZE_LIMIT <= max_dataset_size)
        
        classifier = BernoulliNB()
        # classifier = MultinomialNB()
        # classifier = SGDClassifier()
        # vectorizer = DictVectorizer(dtype=float, sparse=True)
        # vectorizer = create_hashing_vectorizer()

        vectorizer = create_count_vectorizer(database)
        
        processed_posts_iter = create_post_iter(database)
        
        print('Compiling Test Set...')
        test_set = []
        for (i, post) in enumerate(itertools.islice(processed_posts_iter, TEST_SET_SIZE)):                
            if i % 100 == 0:
                print(f'Test Set Iteration: {i}')   
            test_set.append(post)

        print('Vectorizing Test Set...')
        X_test, y_test = list(zip(*test_set))
        X_test = vectorizer.transform(X_test)
        print(f'Test Set Size: {len(test_set)}')
        print()
        
        print('Training...')
        dataset = []
        for (i, entry) in enumerate(itertools.islice(processed_posts_iter, TRAIN_SET_SIZE)):                
            if i % 100 == 0:
                print(f'Progress: {i}')
                
            dataset.append(entry)

            if i % 100 == 0 and i > 0:                      
                print(f'Dataset Size: {len(dataset)}')  
            
                print('Training...')
                X, y = list(zip(*dataset))
                X = vectorizer.transform(X)

                classifier.partial_fit(X, y, classes=np.array([0, 1]))

                accuracy = classifier.score(X_test, y_test)
                print(f'Accuracy: {accuracy}')
                dataset = []
                
        print("Pickling...")        
        with open('model.pkl', 'wb') as file:
            pickle.dump(classifier, file)
        with open('vectorizer.pkl', 'wb') as file:
            # This is only needed for training CountVectorizer,
            # and bloats the pickle.
            # vectorizer.stop_words_ = None
            vectorizer.steps[0][1].stop_words_ = None
            
            pickle.dump(vectorizer, file)

if __name__ == '__main__':
    main()